# Preparing the environment
1. Install python3: ``` $ brew install python3 ```
1. Config python3 as default python:
    ```shell script
       $ sudo nano ~/.zprofile
       $ sudo nano ~/.zshrc
    ```
   
   ```shell script
      # aliases
      alias pip=pip3
      alias python=python3     
   ```
1. Create the VENV: ``` $ virtualenv -p python3 venv ```
1. Activate the VENV: ``` $ source venv/bin/activate ```
1. Install it python's dependencies: ```$ pip install -r requirements.txt```
1. Run it Flask server:  ``` $ FLASK_ENV=development flask run -h 0.0.0.0 ```


# Testando/Executando a Blockchain
1. [Link para minerar um bloco](http://localhost:5000/mine_block)
1. [Link para ver o histórico (chain/blockchain)](http://localhost:5000/blockchain)
1. [Link para verificar se a blockchain é valida (blocos encadeados/interdependentes e atender à regra de desafio proposto)](http://localhost:5000/is_valid)

# Crypto Currency Layers
1. Technology: Strategy / Technique
1. Protocol: define the rules:
    1. how the nodes communicate each other:
        1. Protocols like over HTTP, TCP and so on...
        1. How the message is built, how can it be send one to another, what is the endpoint pattern and so on....
    1. consensus like: 
        1. what is the challenge to be solve
        1. how to validate if the chain is valid or not
1. Tokens: Smart Contracts (DAO) and Tokens.

Examples:
1. Technology: Blockchain, Tangle and so on;
1. Protocol: Waves; Ethereum; Bitcoin; Neo; Ripple;
1. Tokens: __Waves__(WCT,B1,WGR,INTL); __Ethereum__(TRX,AE,REP,SNT,BNB); __Bitcoin__(x); __Neo__(ACAT,TNC,DBC,RPX,QLC,TKY,ONT,IAM);__Ripple__(x).    

Ecosystem:
1. users/peoples
1. miners (solo/standalone machine)
1. big miners (company specialized in mining)
1. pools of miners (standalone machines that share the earnings using a network of miners)


# Monetary politic
1. Halving (when it will gives mid reward to the miner, comparing to it past reward):
    1. 4 years, 210k mined blocks, computed by the software
    1. Samples:
        1. 03/01/2009 - Block (altura): 0 - Reward age: 1 - Reward: BTC/block = 50 BTC
        1. 22/04/2010 - Block: 52500 - Reward age: 1 - Reward: BTC/block = 50 BTC
        1. 28/11/2012 - Block: 210K - Reward age: 2 - Reward: BTC/block = 25 BTC
        1. 29/07/2015 - Block: 367500 - Reward age: 2 - Reward: BTC/block = 25 BTC
        1. 09/07/2016 - Block: 420000 - Reward age: 3 - Reward: BTC/block = 12.5 BTC
        1. 23/06/2017 - Block: 472500 - Reward age: 3 - Reward: BTC/block = 12.5 BTC
        1. 2020 - 6.25 BTC
        1. 2024 - 3.125 BTC
    1. check [blockchain blocks history](http://blockchain.com/pt/explorer) 
    1. [Bitcoin monetary vs FED monetary politic](https://hackernoon.com/this-time-is-different-part-2-what-bitcoin-really-is-ae58c69b3bf0)  
1. How long time for each block


# Mining Difficult
1. Difficult:
    1. new_difficult = current_difficult / max_difficult
    1. sample of actual difficult: 0000000000000005d97dc000000000000000000000000000000000000000000000
    1. sample max difficult: 000000FFFF000000000000000000000000000000000000000000000000000000000000
    1. goal adjusted each 2016 blocks (2 weeks)
    1. can check it current difficult at https://www.blockchain.com/ , example: difficult = 10.771.996.663.680.4
    1. Difficult evolution: "a relative measure of how difficult it is to find a new block", source: [blockchain.com](blockchain.com)
1. justice, but hash power unavailable 
1. Nonce variance:
    1. is a number, non signed, 32bits, 0 to 4 bilhões
    1. difficult:
        1. total valid hashes (18 zeros at the left): 2*10^55
        1. random nonce valid chance: 0,00000000000000002%
    1. hashes available without collision: 4x10^9
    1. valid Nonce probability: 0,00000001%
    1. a small miner produce 100M hashes per second (4B / 100M = 40 seconds generate all hashes to check)
    1. --> that way it is so hard to mining that need the timestamp as workaround to decrease it difficult
        1. timestamp = increasing value that increase for each second, since jan 01 1970
        1. avalanche effect: when the timestamp change the hash change too. So we have 1 second until the timestamp change to find the hash, after that the Nonce will change
            1. have 4Billion of Nonce to test it hash foreach second (timestamp change)
        1. Statistic about hash rate (22Millions of trillions of hash per second): blockchain.info   
        1. __TIMESTAMP IN THE ENCRYPTION IS USEFUL ONLY FOR MINING CRYPTO CURRENCIES AND IT IS A WASTE OF ELECTRIC POWER__, use it just to test consistence


# Mining Pool
*Mining bitcoin is not the best way to get bitcoin, it is better to buy it than mining it
1. a pool of small miners that share ranges of Nonce to mine a bitcoin 
1. distribute Nonce range and each compute calc inside it range to prevent duplicated work inside the mining pool
1. when the bitcoin is found it is shared based on the hash power of the computers, the bigger hash power you have, more percentage will receive
1. there is public pool and private pool, for public pool you can just download it pool software and integrate the network
1. check it public pools at: https://blockchain.com/pools, BTC.com is the biggest 
1. more than 90% of BTC gained from pools, not private miners
1. the most recommended pool to sign (was the first pool of the world): slushpool.com
1. [mining pools per country and more statistics (1. China(81%); 2. Czech Republic(10%); 3. Iceland(2%))](http://buybitcoinworldwide.com/mining/pools)
1. [statistic about bitcoin power (energy) consume](http://powercompare.co.uk/bitcoin)
1. [why bitcoin strategy of mining is so bad](http://blog.bitcoin.org.hk/bitcoin-mining-and-energy-consumption-4526d4b56186)
1. How miners get the transaction:
    1. [charts](https://blockchain.com/charts)
        1. [amount of transactions registered per mined block, sometimes there is no transaction to process and its just mining (power waste)](https://blockchain.com/charts/n-transactions-per-block?timespan=all)
1. __BTC.com Transaction Accelerator: probability of transaction in 1 hour = 75% and in 4 hours to 98%__


# HashPower
1. CPU is Generic it has less(<) than 10MH/s
1. GPU is specialized it has less (<) than 1GH/s
1. ASIC is fully specific for mining it is greater (>) than 1.000 GH/s


# Mempools
1. The participants of a network are known as nodes, classified as users that send transactions and miners
1. Mempool are the pending transactions, every transaction is registered in the mempool waiting for the block to confirm it
1. In the mempool the transaction is disseminated to the network and it priority is based on it paid tx for the transaction confirmation in the blockchain
1. [Mempool Sample](https://blockchain.com/btc/unconfirmed-transactions)
1. ONE BLOCK can have up to 2k of transactions, the mempool increase and decrease based on how much transactions were registered in the block


# Orphan Blocks
1. https://blockchain.com/btc/orphaned-blocks;
1. The orphan blocks are the blocks that was mined at the same index as another block but the hash power (power of propagation) was lower and so the greater hash power propagate FIRST. The first propagated block wins;
1. The transactions in the orphan blocks will come back to the Mempool when it block is confirmed as orphan. It occurs because of the consensus protocol. **This is why is necessary to wait some confirmations before consider it transaction was effective**; 
1. Sometime the same mining pool processed 2 blocks, but in general there are 2 pools generating the same block;
1. The double money spend occurs when u send the product before the confirmation, do not trust on the mempool. It consensus is not efficient.


# Attack 51%
1. Attack blocks are impossible, because you might hack all the computers at the same time;
1. Attack 51% is about hash power. A pool, with bigger hash power, start mining disconnected to the network and it network grow faster than the normal network. 
    1. As it is a distinct network, it is not propagating, so no one knows that it is happening;
    1. After achieve a considerable size the "private attacker network" starts to propagate it blocks to the mainnet;
    1. As the 51% is bigger than the mainnet the consensus accept the new network (attacker) blocks and rollback the blocks of the smaller;
    1. **If a transaction is not processed after 72hours it goes back to it previous owners**.
    

# CryptoCurrencies
1. Transactions & UTXOs
1. Transaction fees
1. How does Wallet work
1. Private & Public keys assigns
1. Digital Sign
1. Segregated Witness (SegWit) - Testemunha Segregada
1. Public Key vs Bitcoin Address
1. Hierarchically Deterministic (HD) Wallets

## Transactions & UTXOs
UTXOs (Unspent Transactions Outputs) = available currencies, not used units (balance)
Example (received transactions):
1. Mark    -> me 0.2 BTC
1. Hadelin -> me 0.3 BTC
1. Helen   -> me 0.6 BTC
1. Susan   -> me 0.7 BTC
1. TOTAL   -> me 1.8 BTC UTXOs
BLOCK 501

Example spending it (buy 0.5 BTC at bike store):
1. buy bike for 0.5 BTC
1. incoming 0.6 from Helen -> 0.5 BTC UTXO to bike store -> 0.1 BTC UTXO comes back to me
BLOCK 502


Example spending it (buy 1.1 BTC at bike store):
1. buy bike for 1.1 BTC
1. new balance (not spent money, check that Helen went away):
   1. Mark                    : 0.2 BTC
   1. Hadelin                 : 0.3 BTC
   1. Susan                   : 0.7 BTC
   1. balance, from last spend: 0.1 BTC
1. spent money: Hadelin (0.3), Susan (0.7), balance{troco} (0.1) = 1.1  
1. UTXO (output) = 1.1 BTC to the bike store
1. new balance after buy the bike (not spent money, check just Mark didnt go away):
   1. Mark: 0.2 BTC
   1. BLOCK 503
1. Every transaction must zero the calc, if more than transaction it % comes back


## Transaction fees
How the fees of the transactions come to the miners. [Transactions Working Sample Link](https://blockchain.com/pt/btc/tree/483743492)
The UTXOs that are inside of a transaction are part of new transactions, everything is connected

History Balance sample (incoming UTXOs):
1. Mark    -> me 0.1 BTC
1. Sarah   -> me 0.1 BTC
1. Hadelin -> me 0.4 BTC
1. Ebay    -> me 0.3 BTC
1. Hadelin -> me 0.3 BTC
TOTAL = 1.2 BTC 
Want to buy (UTXOs spending): 0.9BTC(bike) & 0.02BTC(apple)
TOTAL spend = 0.92 BTC
TOTAL (0.92) - Balance (1.2) = 0.28 BTC
UTXOs back = 0.28

1. me -> bike store  (0.9 BTC)
1. me -> fruit store (0.02 BTC)
1. me -> me (0.28 BTC) 
BLOCK 504
????????? video Aula 46 Udemy, where are the FEEs???


## How does Wallet work
How to create, store balance and so on
1. There is no balance stored, it is calculated passing through all "wallet" transactions (incoming and spending UTXOs) to provide the "balance"
1. Check previous sections to find the blocks 501,502,503 & 504;
1. My wallet blockchain:
    1. block 501
        1. Mark ->      me = 0.1 BTC (UTXO)
        1. Hadelin ->   me = 0.3 BTC (used, unavailable balance, after block 502)
        1. Helen ->     me = 0.6 BTC (used, unavailable balance, after block 502)
        1. Susan ->     me = 0.7 BTC (used, unavailable balance, after block 502)
    1. Block 502
        1. me ->        bike store = 1.1 BTC (used here)
        1. me ->        bike store = 0.5 BTC (used here)
        1. me ->        me = 0.1 BTC (used, unavailable balance, after block 504)
    1. Block 503
        1. Sarah ->     me = 0.1 BTC (UTXO)
        1. Hadelin ->   me = 0.4 BTC (used, unavailable balance, after block 504)
        1. eBay ->      me = 0.3 BTC (used, unavailable balance, after block 504)
        1. Hadelin ->   me = 0.3 BTC (used, unavailable balance, after block 504)
    1. Block 504
        1. me ->        bike store = 0.9 BTC (used here)
        1. me ->        fruit store = 0.02 BTC (used here)
        1. me ->        me = 0.06 BTC (UTXO, rest of it transaction)
    1. Balance (sum UTXOs): 0.1 BTC (Mark - UTXO block 501) + 0.1 BTC (Sarah - UTXO block 503) + 0.06 BTC (UTXO block 504) = 0.26 BTC balance 


## Private & Public keys assigns
Work flow:
- Private key -> 
    - (generated) Public Key -> 
        - Verification Function (uses public key) -> yes/no (boolean output)
    - Signature -> 
        - Verification Function (uses public key) -> yes/no (boolean output)
        - Transaction ->
            - Verification Function (uses public key) -> yes/no (boolean output)
- Transaction (send UTXO to a) ->
    - Private key
What blockchain really use: public key, transaction & signature. What blockchain does with it: verification function

## Digital Sign & keys demonstration
1. Check the ```./demos/public-private-key-demo-master``` project folder
1. KEYS
    1. values overview (obs.: private can be anything and public is generated, the most it private is larger more secure it is):
        1. the private key is just numeric, like: 59587012902816325860124193377579404157291272102688524473445797894021989073574
        1. the public key is alphanumeric and larger, like: 04e5a52653d845c64822fbb4128183683d93d6a1ead86d121da21e1c5a700483b500c163a8e4c0394184862c4bf0cee799f5d7c8bc29709ee37c501f7e5bbb6cdf 
1. SIGNATURE
    1. SIGN -> **use private key**
        1. add some message, like: JOSE -> 4 BTC PARA FERNANDO
        1. private key already filled
        1. click "sign"
        1. check it output at "Message Signature"
    1. VERIFY -> **use public key** 
            1. **do not use private key, because anyone can verify any signature message** (everything comes filled)
        1. add the same message
        1. add public key
        1. add message signature
        1. verify
1. TRANSACTION -> any input change, change the output
    1. SIGN
        1. amount of "bitcoin/currency"
        1. from = public compatible with the private
        1. -> (to) = public destiny
        1. private key = the entry private key 
        1. **output** = Message Signature
    1. VERIFY
        1. same values from the sign (autofill)
        1. signature was generated in the sign action
        1. click verify
1. BLOCKCHAIN
    1. each block has:
        1. index (#)
        1. Nonce (proof of work generated/mined for each block)
        1. Coinbase ???????? - Aula 49 (the monetary coins, like: 100 (default), so cant spend more than 100 coins??? in the test it failed)
        1. Tx (transactions)
        1. Prev. Hash
        1. Block Hash


## Segregated Witness (SegWit) - Testemunha Segregada
The block max_size is 1mb, if lower it wont support a lot of transactions per block, if greater it will slowdown the network.
This created the need to tryout a fork:
    - hard (creating incompatibility with existing network) = increase block size (Bitcoin Cash) 
    - soft (change the func keeping compatibility) = SegWit (Bitcoin)
ScriptSig = Signature & Public Key -> up to 60% of the block, the rest is: "from, to, hash & value" (40%).
SegWit is send the ScriptSig outside of the bock, but linked to the block.


## Public Key vs Bitcoin Address
Check: ["What's the difference between public key and public address?" - by hksupport (2016)](https://www.reddit.com/r/Bitcoin/comments/3filud/whats_the_difference_between_public_key_and/)
- PeerA:
    - 08ABF3A4(private key) -> 9OBE87AD(public key) -> 86AE25FA(bitcoin public address) 
- PeerB:
    - E5638CB7(private key) -> 7731EF12(public key) -> ADE6531B(bitcoin public address)


## Hierarchically Deterministic (HD) Wallets
Using the consumption patterns like movies, trade with some group of people, items/services often bought by same people and so on, create a pattern of "who am i".      
[Deterministic Wallets, their advantages and their understated flaws](https://bitcoinmagazine.com/articles/deterministic-wallets-advantages-flaw-1385450276)
??????????????? aula 52  

# Crypto Currency
To add the currency to the blockchain is possible to use the previous generic blockchain created and just add the **Consensus Algorithm** and the **Transactions Algorithm**
- Transactions = UTXOs logic
    - add transactions to a list and foreach created block add it transactions list to the block transactions attr and reset it list
- Network
    - each node must have a attr called nodes which stores all others nodes to propagate the data
    - each node must tell the others that he is coming to the network (register it self), it might just pass it address (ip:port) 
- Consensus = equalize the nodes to sync a blockchain ledger history based on winners blocks and ignore orphans
    - replace the chain based on it bigger valid chain
- Testing it:
    1. **Startup**: run 3 flask server changing just the port number
    1. if test /blockchain request the chains are diff, but after the sync action the consensus stabilize it for the longer chain and everyone will be the same
    1. **Register Nodes**: Perform post request passing the nodes.json template to each server (delete the line that represents the server receiving the request (check postman))
    1. **Mine a block**: perform get request to /mine_block just in one server instance
    1. **Sync Chains**: perform get request to /sync_chain just in all server instance (the mined is not necessary) 
    1. **Test Chains**: foreach instance perform /blockchain and every genesis block will be the same and the mined blocks too
    1. **Add transaction**: perform post request to /add_transaction with the transaction.json template like:
        ```json
            {
              "sender": "Ilton",
              "receiver": "Lidia",
              "amount": 1000
            }
        ```
    1. **Mine a new block**: perform get request to /mine_block just in one server instance, it prev transaction will be registered in the transaction
    1. **Sync Chains**: perform get request to /sync_chain just in all server instance (the mined is not necessary)
    1. **Test new Chains**: foreach instance perform /blockchain and every genesis block will be the same and the mined blocks too 


# Smart Contract
1. Bitcoin allows smart contracts, but not as tokens like Ethereum
1. Bitcoin Script is not Turing Complete (missing loops), the Solidity (Ethereum Lenguage) is Turing Complete
    1. In distribute computing a infinity loop can be a nightmare
1. Each node will retain:
    1. History of smart contracts
    1. History of transactions (currency, like ether)
    1. Current state of all smart contracts (tokens)
1. [Supply Chain smart contract capability](https://provenance.org/whitepaper)
1. [Smart Contract for Dummies](https://freecodecamp.org/news/smart-contracts-for-dummies-a1ba1e0b9575)
1. DecentralizedApplications **DApps**:
    1. The smart contract is an API for applications that uses blockchain;
    1. DApps Always have a backend & a frontend; 
    1. DApps frontend is an application connected through API to the backend - contract (blockchain app code);
    1. Example of DApp: [Steemit](https://steemit.com)
1. Ethereum Virtual Machine (EVM) & Gas:
    1. The larger complexity and popular more vulnerability opportunities;
    1. [Gas History/Workflow](https://ethgasstation.info/gasguzzlers.php)
    1. [Calculate Costs and how does it work in Ethereum Smart Contract](https://hackernoon.com/ether-purchase-power-df40a38c5a2f)
1. Decentralized Autonomous Organizations **DAOs**:
    1. Organization is people working together following terms (processes/protocols) to operate it in group;
    1. Each Smart Contracts represent a process/protocol, a collection of it SC creates a DAO;
    1. Once the rules were written it might work autonomous, without human intervention. To update it rule might have a consensus;
    1. [DAOs, DACs, DAs and more: an incomplete terminology guide](https://blog.ethereum.org/2014/05/06/daos-dacs-das-and-more-an-incomplete-terminology-guide)
1. DAO Attack:
    1. The first created DAO was the Ethereum, created in 2016
    1. Funded by the biggest crowd funding in the history, may 2016 amount: US$ 150 Millions
    1. June 2016 hacked in US$ 50 Millions, the Ethereum DAO, not the Ether, had a logic "bug" and a hacker converted it logic to withdraw third-party money
    1. To rollback it was made a hard fork creating ETH and the previous mainline is now known as ETC
    1. The hackers got about US$67Millions in ETC 

# Forks
1. Every update in the rules of the blockchain can generate a fork, if everyone update, there is no fork, is someone dont update it is a fork, but if it doesnt crash the mining logic it node still working
1. If the rule change and generate incompatibility it generate a new coin, because it new blockchain line is not generating blocks that deal as valid to the other
    1. This is why u earn the new coins when it happens, because your old balance is still valid
 
# ICOs vs IPOs
1. People create a product
    1. Generate a company
        1. offer assets (actions or Tokens)
            1. Investors receive (tokens)
                1. Company generate profit
                    1. investors receive earnings (bitcoin, eth, cash...)
1. Tokens are not mined, but can expand it
1. Analysing ICOs:
    1. check if it really decentralized
    1. check how it token is useful (has demand)
    1. is it the usage is really useful on blockchain instead centralized option?
1. Link: [WTF is ICO?](https://techcrunch.com/2017/05/23/wtf-is-an-ico)
1. **Case Study of an ICO**:
    1. Goal: create a Roller Custer, needs example amount US$ 1Million
        1. Create a Token (SToken name), generate 1 million of supply
            1. The creator hold 0.5 Million
            1. 0.5 Million available for trade/investors 
    1. Usage: use this token to use the Roller Custer
    1. Pricing: US$ 2 for investors and US$ 6 after Roller Custer launch for users/customers
    1. Users use the token to enjoy the Roller Custer and investors can sell it tokens to those people
    1. **Tokens are used to be consumed, the crypto currency is to perform actions in the network**
    1. [It example link](https://hackernoon.com/what-the-heck-is-an-ico-6f3736d5f5a)
    1. [Another Sample (How cripto tokens can disrupt Uber and AirBnb)](https://finnscave.com/2018/02/07/how-crypto-tokens-will-enable-the-disruption-of-business-like-uber-and-airbnb)
1. ICO StartUps WhitePapers:
    1. [Critics useful for parallel's tokens (Useless Ethereum Token)](https://uetoken.com)
1. Blockchain & Web 3.0:
    1. Internet in 90's (amazon and apple) vs today sites (apple and amazon)
    1. Probably we still in 90's for blockchain technology
    1. [Centralized vs Decentralized services options](https://medium.com/@matteozago/why-the-web-3-0-matter-and-you-should-know-about-it-a5851d63c949)
        1. **EXAMPLES POSSIBLES FOR parallel**:
            1. (BROWSER) Google Chrome **vs** Brave
            1. (STORAGE) Dropbox & Google Drive **vs** Storj & IPFS
            1. (VIDEO&AUDIO CALLS) Skype **vs** Experty
            1. (Operating System) Android & iOS **vs** Essentia.one & EOS
            1. (Social Network) Facebook & Twitter **vs** Steemit & Akasha
            1. (Messaging) Whatsapp & Telegram **vs** Status
            1. (Remote Job) Up & Workona **vs** Ethlance
             

## How to Soft Fork
To soft fork a crypto currency we need to know just a few things about the network behaviour:
1. How it genesis block is created 
1. Found out how the chain is stored and the method used to generate a new blank chain
1. How it currencies are mined (master nodes are just pre mined currencies)
    1. for masternodes blockchains is necessary to find the genesis declaration and it hardcoded hashes, 
    those hardcoded hashes are the **public address** that owns the main currencies pool, to change it do:
        1. generate a private key (secure store it)
        1. find the method that generate it public key and use the private key to generate it public key and store it
        1. find the method that generate it public address and use it generated public key to generate it public address
        1. waves sample: 
            ``` 
                # Configuration for genesis block generator
                # To generate run from SBT:
                $ node/test:run node/src/test/resources/genesis.it.conf 
            ``` 
1. How the nodes register itself to the network (params, connection protocol (http, tcp...))
1. How is the consensus protocol
1. ICO StartUps WhitePapers:
    1. [Critics useful for parallel's tokens (Useless Ethereum Token)](https://uetoken.com):
        1. **ethereum**: ICOs slow down Ethereum network | **parallel**: it hype speed up the network
        1. **ethereum**: Depressive people with they loses at /r/ethtrader, nothing useful was really made with the Token | **parallel**: some tokens, like for franchising is verified & certified if the launcher request for it
        1. **ethereum**: The most ethereum Smart Contracts were writen as copy of another, and the launcher has no idea what he is doing | **parallel**: simple tokens are created in few clicks, just DAOs uses SmartContract code
        1. **ethereum**: The most ethereum tokens doesn't find an exchange to be traded | **parallel**: any token/smartcontract is launched at parallel dex
        1. **ethereum**: To move tokens u need to spend money and it is slow | **parallel**: To move tokens u spend money, but to move currency u dont and the most it is done faster it is
        1. **ethereum**: These tokens just can be held or send, nothing else | **parallel**: Tokens can be verified to real goods like buy a sandwich at McDonald's Store that sold it and so on


## parallel consensus
1. Dont use timestamp, use mempool instead. Each 5 transactions a block is mined, as more transactions received more faster the blocks will be calc
1. The winner of the 5 transaction fees will be the first node to propagate the new block, the winner will be the faster network not hash power
    1. this strategy allows nearest miner to get the transactions nearest and mine it and aggregate to the main blockchain
1. Even smartphones can earn transactions fees
 
 #TODO: can eliminate the transaction fee for transactions in the main currency and get it just for trade or smart contracts? 
