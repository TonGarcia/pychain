
import json
from uuid import uuid4
from cryptocurrency.hadcoin import HadCoin
from flask import Flask, jsonify, request


blockchain = HadCoin()
app = Flask(__name__)

node_address = str(uuid4()).replace('-', '')


@app.route('/mine_block', methods=['GET'])
def mine_block():
    """
        Mine a new block (calc it hash and register it mempool transactions to the block)
        :return: 200 mined block data
    """
    previous_block = blockchain.get_previous_block()
    previous_proof = previous_block['proof']
    proof = blockchain.proof_of_work(previous_proof)
    previous_hash = blockchain.hash(previous_block)
    # it will add the cryptocurrency to the miner
    blockchain.add_transaction(sender=node_address, receiver='Ilton', amount=blockchain.miner_reward)
    block = blockchain.create_block(proof, previous_hash)
    response = block
    response['message'] = 'Congratulations, you mined a block!'
    return json.dumps(response)


@app.route('/blockchain', methods=['GET'])
def get_chain():
    """
        Return the complete blockchain data
        :return: 200 with all blockchain data
    """
    blockchain_chain = blockchain.chain
    response = {
        'chain': blockchain_chain,
        'length': len(blockchain_chain)
    }
    return json.dumps(response)


@app.route('/is_valid', methods=['GET'])
def is_valid():
    """
        Check if the chain is valid, based on it network rules
        :return: 200 with message saying if it chain is valid or not
    """
    if blockchain.is_chain_valid(blockchain.chain):
        response = {'valid': True, 'message': 'Successful, the blockchain is valid!'}
    else:
        response = {'valid': False, 'message': 'Error, the blockchain is invalid!'}
    return json.dumps(response)


@app.route('/nodes', methods=['GET'])
def nodes():
    """
        List all connected/registered nodes
        :return: list of nodes
    """
    return json.dumps(list(blockchain.nodes))


@app.route('/mempool', methods=['GET'])
def mempool():
    """
        :return: list of transactions available for the next block (mempool)
    """
    return json.dumps(blockchain.transactions)


@app.route('/add_transaction', methods=['POST'])
def add_transaction():
    """
        Add a new transaction to the mempool and it is inserted in the block when it is mined
        :return: 400 if missing params
    """
    req_json = request.get_json()
    transaction_keys = ['sender', 'receiver', 'amount']
    # check params
    if not all(key in req_json and len(str(req_json[key])) > 0 for key in transaction_keys):
        m = 'Some attrs are missing, check your request params. Expected attrs: sender, receiver and amount'
        return json.dumps({'message': m}), 400
    index = blockchain.add_transaction(req_json['sender'], req_json['receiver'], req_json['amount'])
    resp = {'block_index': index, 'message': f'This transaction will be added to the block #{index}'}
    # 201 HTTP status code means new resource created successfully
    return json.dumps(resp), 201


@app.route('/connect_nodes', methods=['POST'])
def connect_nodes():
    """
        Check new nodes list received
        :return: existing nodes + received new nodes
    """
    req_json = request.get_json()
    req_nodes = req_json.get('nodes')
    if req_nodes is None:
        return 'missing nodes attr', 400
    for node in req_nodes:
        blockchain.add_node(node)
    resp = {
        'message': 'All nodes registered and connected',
        'nodes': list(blockchain.nodes)
    }
    return json.dumps(resp), 201


@app.route('/sync_chain', methods=['GET'])
def sync_chain():
    """
        Iterate all the network nodes checking if there is a newer longer chain and replace if there is a valid longer
        :return: 201 if replaced and 200 if not replaced
    """
    synced_chain = blockchain.sync_chain()
    resp = {'chain': blockchain.chain}

    if synced_chain:
        resp['message'] = 'the nodes has diff chains, so it chain was synced'
        return json.dumps(resp), 201
    else:
        resp['message'] = 'the chain was up to date, nothing changed'
        return json.dumps(resp), 200




# external access to decentralize it
app.run(host='0.0.0.0', port=5000)
